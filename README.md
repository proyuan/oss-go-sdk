# oss

### 介绍
oss 文件上传
### 安装
```go
go get gitee.com/proyuan/oss-go-sdk
```
### 使用示例

```go
package main

import (
	"gitee.com/proyuan/oss-go-sdk/oss"
	"io"
)

func main() {
	client, err := oss.New("youEndpoint", "youAccessKeyId", "youAccessKeySecret")
	if err != nil {
		return
	}
	bucket, err := client.Bucket("youBucket")
	if err != nil {
		return
	}
	// SampleFromFile 简单上传通过本地文件
	err = bucket.SampleFromFile("youObjectKey", "youLocalFileDir")
	if err != nil {
		return
	}

	// SampleFromFileStream 简单上传通过文件流
	err = bucket.SampleFromFileStream("youObjectKey", io.Reader)
	if err != nil {
		return
	}

	// SampleFromByteArr 简单上传通过字节数组
	err = bucket.SampleFromByteArr("youObjectKey", []byte{})
	if err != nil {
		return
	}
}

```
