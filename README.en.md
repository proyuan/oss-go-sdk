# oss

### Desc
oss file upload
### Installation
```go
go get gitee.com/proyuan/oss-go-sdk
```
### Example

```go
package main

import (
"gitee.com/proyuan/oss-go-sdk/oss"
"io"
)

func main() {
client, err := oss.New("youEndpoint", "youAccessKeyId", "youAccessKeySecret")
if err != nil {
return
}
bucket, err := client.Bucket("youBucket")
if err != nil {
return
}
// SampleFromFile sample upload from local file
err = bucket.SampleFromFile("youObjectKey", "youLocalFileDir")
if err != nil {
return
}

// SampleFromFileStream sample upload from file stream
err = bucket.SampleFromFileStream("youObjectKey", io.Reader)
if err != nil {
return
}

// SampleFromByteArr sample upload from byte array
err = bucket.SampleFromByteArr("youObjectKey", []byte{})
if err != nil {
return
}
}

```
