package oss

import (
	"bytes"
	aliOss "github.com/aliyun/aliyun-oss-go-sdk/oss"
	"io"
)

type Bucket struct {
	bucket *aliOss.Bucket
}

// SampleFromFile 简单上传通过本地文件
func (b *Bucket) SampleFromFile(objectKey, dst string) error {
	err := b.bucket.PutObjectFromFile(objectKey, dst)
	if err != nil {
		return err
	}
	return nil
}

// SampleFromFileStream 简单上传通过文件流
func (b *Bucket) SampleFromFileStream(objectKey string, stream io.Reader) error {
	err := b.bucket.PutObject(objectKey, stream)
	if err != nil {
		return err
	}
	return nil
}

// SampleFromByteArr 简单上传通过字节数组
func (b *Bucket) SampleFromByteArr(objectKey string, byteArr []byte) error {
	err := b.bucket.PutObject(objectKey, bytes.NewReader(byteArr))
	if err != nil {
		return err
	}
	return nil
}
