package oss

import aliOss "github.com/aliyun/aliyun-oss-go-sdk/oss"

type Oss struct {
	client *aliOss.Client
}

func New(endpoint, accessKeyID, accessKeySecret string, options ...aliOss.ClientOption) (*Oss, error) {
	client, err := aliOss.New(endpoint, accessKeyID, accessKeySecret, options...)
	if err != nil {
		return nil, err
	}
	return &Oss{client: client}, nil
}

func (o *Oss) Bucket(bucketName string) (*Bucket, error) {
	bucket, err := o.client.Bucket(bucketName)
	if err != nil {
		return nil, err
	}
	return &Bucket{bucket: bucket}, nil
}
